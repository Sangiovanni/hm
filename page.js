
$(document).ready(function () {

    //default values

    var editorselectedval = "4"; //indicare quello di default usato nel codice html.
    var dateselected = "26.08.2018";

    $("#editor1").Editor();
    $("#editor3").Editor();
    $("#editor4").Editor();
    $("#editor5").Editor();
    $("#editor6").Editor();
    
     $('#date').val(dateselected);

    $('#date').datepicker({
        format: 'dd.mm.yyyy',
        language: 'de',
        todayHighlight: true,
        autoclose: true
    }).on('changeDate', function(ev){
        dateselected =  $('#date').val();
        loadfile();
    });

    editorSelect(editorselectedval);




    $("#save-btn").click(function () {
//alert($('#editor1').html());
        //alert( quill1.container.firstChild.innerHTML);
        var arr_selected = editorselectedval.split("_");
        for (var i = 0, len = arr_selected.length; i < len; i++) {

            $.post("ajax.php", {"option": "save", "htmltext": $('#editor' + arr_selected[i]).Editor("getText"), "filename": "test" + dateselected + "_"+ arr_selected[i]})
                    .done(function (data) {
                        //loadfile();
                    });
        }
    });


    //load file

    function loadfile() {
        var arr_selected = editorselectedval.split("_");
        

        for (var i = 0, len = arr_selected.length; i < len; i++) {

            $.ajax({
                type: 'POST',
                url: "ajax.php",
                data: {"option": "load", "filename": "test" + dateselected + "_"+ arr_selected[i]},
                async: false,

            }).done(function (data) {
                $('#editor' + arr_selected[i]).Editor("setText", data);
            });


        }
    }



    function editorSelect(edn) {
        editorselectedval = edn;

        $(".allcontents").hide();

        $("#cont_editor" + edn).show();

        loadfile();
    }

//  ONCHANGE EVENTS

    $('input[type=radio][name=editList]').change(function () {
        switch (this.value) {
            case "Deutsch":
                editorSelect("1");
                break;
            case "IT/DE":
                editorSelect("5_6");
                break;
            case "Italiano":
                editorSelect("3");
                break;
            case "Canti":
                editorSelect("4");
                break;
            default:
                break;
        }
    });
    
    




});//doc.ready
  